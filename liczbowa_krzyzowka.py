import sys
from colorama import init, Fore
init()

class NumericalCrossword(object):
    def __init__(self):
        self.__BOARD = [
          'XX     X      X',
          '   X X   X X XX',
          ' X     X       ',
          ' X X X X X X X ',
          '    X     X    ',
          ' X X X X X X X ',
          '       X       ',
          'XXXX XXXXX XXXX',
          '       X       ',
          ' X X X X X X X ',
          '    X     X    ',
          ' X X X X X X X ',
          '       X     X ',    
          'XX X X   X X   ',
          'X     X      XX']
        self.board = []
        for line in self.__BOARD:
            a = []
            for char in line:
                a.append(char)
            self.board.append(a)
        self.width = len(self.board[0])
        self.height = len(self.board)
        self.numbers3 = []
        self.numbers4 = []
        self.numbers5 = []
        self.numbers6 = []
        self.numbers7 = []
        
    def ClearBoard(self):
        for i in range(self.height):
            for j in range(self.width):
                if self.board[i][j] <> 'X':
                    self.board[i][j] = ' '

    def ShowBoard(self, char1='+', char2='.', numbers = True):
        print '+' + '-'*self.width + '+'
        for i, b in enumerate(self.board):
            sys.stdout.write('|')
            for x in b:
                if x == 'X':
                    sys.stdout.write(char1)
                elif x == ' ':
                    sys.stdout.write(char2)
                else:
                    sys.stdout.write(x)
            sys.stdout.write('|')
            print ''
        print '+' + '-'*self.width + '+'
        
        #print(Fore.RED + '' + Fore.RESET)
        print 'Liczby trzycyfrowe:'
        print ', '.join(self.numbers3)
        print 'Liczby czteroycyfrowe:'
        print ', '.join(self.numbers4)
        print 'Liczby pieciocyfrowe:'
        print ', '.join(self.numbers5)
        print 'Liczby czesiocyfrowe:'
        print ', '.join(self.numbers6)
        print 'Liczby siedmiocyfrowe:'
        print ', '.join(self.numbers7)

    def PutBlockOnBoard(self, block, x=0, y=0):
        if self.CanPutBlockOnBoard(block, x, y):
            for i, line in enumerate(block):
                for j, char in enumerate(line):
                    if char <> ' ':
                        self.board[y+i][x+j] = char
            return True
        else:
            return False

    def IsPointOnBoard(self, x, y):
        max_x = len(self.board)-1
        max_y = len(self.board[0])-1
        return (x >= 0) and (x <= max_x) and (y >= 0) and (y <= max_y)  
    
    def CanPutOnPoint(self, y, x):
        ok = True
        if self.IsPointOnBoard(y-1,x-1):
            ok = ok and self.board[y-1][x-1] <> 'X'
                        
        if self.IsPointOnBoard(y-1,x):
            ok = ok and self.board[y-1][x] <> 'X'

        if self.IsPointOnBoard(y-1,x+1):
            ok = ok and self.board[y-1][x+1] <> 'X'

        if self.IsPointOnBoard(y,x-1):
            ok = ok and self.board[y][x-1] <> 'X'

        if self.IsPointOnBoard(y,x+1):
            ok = ok and self.board[y][x+1] <> 'X'

        if self.IsPointOnBoard(y+1,x-1):
            ok = ok and self.board[y+1][x-1] <> 'X'

        if self.IsPointOnBoard(y+1,x):
            ok = ok and self.board[y+1][x] <> 'X'

        if self.IsPointOnBoard(y+1,x+1):
            ok = ok and self.board[y+1][x+1] <> 'X'
        return ok
    
    def CanPutBlockOnBoard(self, block, x=0, y=0):
        width = len(self.board[0])
        height = len(self.board)
        for i, line in enumerate(block):
            for j, char in enumerate(line): 
                if char == 'X':
                    if (y+i < 0) or (y+i >= height):
                        return False
                    if (x+j < 0) or (x+j >= width):
                        return False
                    if not self.CanPutOnPoint(y+i,x+j):
                        return False
        return True

    def PutNumber(self, x, y, Horizontal, number):
        for i, char in enumerate(number):
            if Horizontal:
                if self.board[y][x+i] <> 'X':
                    self.board[y][x+i] = char
                else:
                    return False            
            else:
                if self.board[y+i][x] <> 'X':
                    self.board[y+i][x] = char
                else:
                    return False
        return True
    
    def LiczbowaKrzyzowkaI(self):
        self.ClearBoard()
        self.PutNumber(8, 0, False, '4251622')
        self.numbers3 = ['187','652','765','963']
        self.numbers4 = ['1908','2609','3313','4676','5212','6407','7024','8241']
        self.numbers5 = ['19351','24007','30442','41693','50289','69734','72268',
                         '75913','87126','93775']
        self.numbers6 = ['129194','272969','435432','501123']
        self.numbers7 = ['1498016','1781640','2456733','3482667','4251622',
                         '4968451','5693211','5916894','6573079','7019855',
                         '7124356','8027294','9313857','9758036']

if __name__ == '__main__':
    board = NumericalCrossword()
    board.LiczbowaKrzyzowkaI();
    board.ShowBoard(chr(219),' ', True)