import sys
from Tkinter import *
from liczbowa_krzyzowka import NumericalCrossword

class TkNumericalCrossword(NumericalCrossword):
    def OnRectangleClick(self, event):
        print "clicked at", event.x, event.y
        canvas = event.widget
        item = canvas.find_closest(event.x, event.y)
        canvas.itemconfig(item, fill="blue")
        #self.w.itemconfig(event.widget, fill="blue") 
        
    def ShowBoardTk(self, numbers = True):
        master = Tk()

        size = 20
        x = size
        y = size
        #width = len(self.board[0])
        #height = len(self.board)

        self.w = Canvas(master, width= x+size*(self.width+1), height = y+size*(self.height+1) )
        self.w.pack()

        # wyswietlenie tabel
        for i in range(self.width):
            for j in range(self.height):
                if (self.board[j][i] <> 'X'):
                    f = 'white'
                else:
                    f = 'black'
                r = self.w.create_rectangle(x+i*size, y+j*size, x+(i+1)*size, y+(j+1)*size, fill=f,
                                   activefill='yellow')
                self.w.tag_bind(r, "<Button-1>", self.OnRectangleClick)

        # wyswietlenie uzupelnionych juz liczb
        for i in range(self.width):
            for j in range(self.height):
                if (self.board[j][i] >= '1' and self.board[j][i] <= '9'):
                    print 'test'
                    self.w.create_text(x+i*size+size/2, y+j*size+size/2, 
                        text = self.board[j][i], font = (None, 12, 'bold'))


        mainloop()

        '''
        print '+' + '-'*self.width + '+'
        for i, b in enumerate(self.board):
            sys.stdout.write('|')
            for x in b:
                if x == 'X':
                    sys.stdout.write(char1)
                elif x == ' ':
                    sys.stdout.write(char2)
                else:
                    sys.stdout.write(x)
            sys.stdout.write('|')
            print ''
        print '+' + '-'*self.width + '+'
        
        #print(Fore.RED + '' + Fore.RESET)
        print 'Liczby trzycyfrowe:'
        print ', '.join(self.numbers3)
        print 'Liczby czteroycyfrowe:'
        print ', '.join(self.numbers4)
        print 'Liczby pieciocyfrowe:'
        print ', '.join(self.numbers5)
        print 'Liczby czesiocyfrowe:'
        print ', '.join(self.numbers6)
        print 'Liczby siedmiocyfrowe:'
        print ', '.join(self.numbers7)
        '''

if __name__ == '__main__':
    board = TkNumericalCrossword()
    board.LiczbowaKrzyzowkaI();
    board.ShowBoard(chr(219),' ', True)
    board.ShowBoardTk(True)