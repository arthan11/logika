from Tkinter import *
from random import randint
from bloki_na_siatce import BlocksBoard

class TkBlocksBoard(BlocksBoard):
    def ShowBoardTk(self, numbers = True, blocks = False):
        master = Tk()

        size = 20
        x = size
        y = size
        width = len(self.board[0])
        height = len(self.board)

        if numbers:
            col_counts, col_max_counts = self.GetColumnsCounts(False)
            row_counts, row_max_counts = self.GetRowsCounts(False)
            x = size*(row_max_counts+1)
            y = size*(col_max_counts+1)
        
        w = Canvas(master, width= x+size*(width+1), height = y+size*(height+1) )
        w.pack()

        # wyswietlenie liczb kolumn  wierszy
        if numbers:
            for j in range(col_max_counts):
                for i in range(len(col_counts)):
                    if j < len(col_counts[i]):
                        w.create_text(x+size*i+size/2, y-size*j-size/2, 
                            text = str(col_counts[i][j]), font = (None, 14))
                                
            for j in range(row_max_counts):
                for i in range(len(row_counts)):
                    if j < len(row_counts[i]):
                        w.create_text(x-size*j-size/2, y+size*i+size/2, 
                            text = str(row_counts[i][j]), font = (None, 14))
        
        # wyswietlenie tabel
        for i in range(width):
            for j in range(height):
                if (not blocks or self.board[j][i] == ' '):
                    f = 'white'
                else:
                    f = 'black'
                w.create_rectangle(x+i*size, y+j*size, x+(i+1)*size, y+(j+1)*size, fill=f)
        



        mainloop()

if __name__ == '__main__':
    board = TkBlocksBoard()
    #board.BlokiNaSiatceII();
    board.BlokiTest()
    board.ShowBoard(chr(219), ' ', False, True)
    board.ShowBoardTk(True, False)