import sys
from random import randint

WIDTH = 11
HEIGHT = 16

class BlocksBoard(object):
    def __init__(self):
        self.board = []
        self.blocks = []
        self.blocks.append(['X  ','XXX','X  '])
        self.blocks.append(['XX','XX','X '])
        self.blocks.append([' X  ','XXXX'])
        self.blocks.append(['XXXX','X   '])
        self.blocks.append([' X','XX','X ', 'X '])
        self.blocks.append(['XX',' X','XX'])
        self.blocks.append(['  X','XXX', ' X '])
        self.blocks.append([' X ','XXX', ' X '])
        self.blocks.append(['XXX','X  ', 'X  '])
        self.blocks.append(['XXXXX'])
        self.blocks.append(['  X','XXX', 'X  '])
        self.blocks.append(['X  ','XX ', ' XX'])
        #for i in range(12):
        #    self.blocks.append([' XX  XX ','XXXXXXXX', 'XXXXXXXX', ' XXXXXX ', '  XXXX  ', 
        #        '   XX   '])
        
    def ClearBoard(self, width, height):
        self.board = [[' ' for i in range(width)] for j in range(height)]

    def GetColumnsCounts(self, First = True):
        # zliczanie kolumn
        counts = []
        for j in range(len((self.board[0]))):
            col_counts = []
            cc = 0
            for i in range(len((self.board))):
                if self.board[i][j] <> ' ':
                    cc = cc + 1
                else:
                    if cc > 0:
                        if First:
                            col_counts.append(cc)
                        else:
                            col_counts.insert(0, cc)
                        cc = 0
            if cc > 0:
                if First:
                    col_counts.append(cc)
                else:
                    col_counts.insert(0, cc)
                cc = 0
            counts.append(col_counts)
        # zliczanie ile jest najwiecej liczb w kolumnie
        max_counts = 0                  
        for c in counts:
            if len(c) > max_counts:
                max_counts = len(c)
        return counts, max_counts

    def GetRowsCounts(self, First = True):
        # zliczanie wierszy
        counts = []
        for j in range(len((self.board))):
            row_counts = []
            cc = 0
            for i in range(len((self.board[0]))):
                if self.board[j][i] <> ' ':
                    cc = cc + 1
                else:
                    if cc > 0:
                        if First:
                            row_counts.append(cc)
                        else:
                            row_counts.insert(0, cc)
                        cc = 0
            if cc > 0:
                if First:
                    row_counts.append(cc)
                else:
                    row_counts.insert(0, cc)
                cc = 0
            counts.append(row_counts)
        # zliczanie ile jest najwiecej liczb w kolumnie
        max_counts = 0                  
        for c in counts:
            if len(c) > max_counts:
                max_counts = len(c)
        return counts, max_counts


    def ShowBoard(self, char1='+', char2='.', numbers = True, blocks = False):
        if numbers:
            counts, max_counts = self.GetRowsCounts()

        print '+' + '-'*len(self.board[0]) + '+'
        for i, b in enumerate(self.board):
            sys.stdout.write('|')
            for x in b:
                if x <> ' ':
                    if blocks:
                        sys.stdout.write(char1)
                    else:
                        sys.stdout.write(char2)
                else:
                    sys.stdout.write(char2)
            sys.stdout.write('|')
            if numbers:
                for count in counts[i]:
                    print count,
            print ''
        print '+' + '-'*len(self.board[0]) + '+'

        # wyswietlenie podsumowania kolumn
        if numbers:
            counts, max_counts = self.GetColumnsCounts()
            for j in range(max_counts):
                sys.stdout.write(' ')
                for i in range(len(counts)):
                    if j < len(counts[i]):
                        #if counts[i][j] > 0:
                            sys.stdout.write(str(counts[i][j]))
                        #else:
                        #    sys.stdout.write(' ')
                    else:
                        sys.stdout.write(' ')
                print ''

    def PutBlockOnBoard(self, block, x=0, y=0):
        if self.CanPutBlockOnBoard(block, x, y):
            for i, line in enumerate(block):
                for j, char in enumerate(line):
                    if char <> ' ':
                        self.board[y+i][x+j] = char
            return True
        else:
            return False

    def IsPointOnBoard(self, x, y):
        max_x = len(self.board)-1
        max_y = len(self.board[0])-1
        return (x >= 0) and (x <= max_x) and (y >= 0) and (y <= max_y)  
    
    def CanPutOnPoint(self, y, x):
        ok = True
        if self.IsPointOnBoard(y-1,x-1):
            ok = ok and self.board[y-1][x-1] <> 'X'
                        
        if self.IsPointOnBoard(y-1,x):
            ok = ok and self.board[y-1][x] <> 'X'

        if self.IsPointOnBoard(y-1,x+1):
            ok = ok and self.board[y-1][x+1] <> 'X'

        if self.IsPointOnBoard(y,x-1):
            ok = ok and self.board[y][x-1] <> 'X'

        if self.IsPointOnBoard(y,x+1):
            ok = ok and self.board[y][x+1] <> 'X'

        if self.IsPointOnBoard(y+1,x-1):
            ok = ok and self.board[y+1][x-1] <> 'X'

        if self.IsPointOnBoard(y+1,x):
            ok = ok and self.board[y+1][x] <> 'X'

        if self.IsPointOnBoard(y+1,x+1):
            ok = ok and self.board[y+1][x+1] <> 'X'
        return ok
    
    def CanPutBlockOnBoard(self, block, x=0, y=0):
        width = len(self.board[0])
        height = len(self.board)
        for i, line in enumerate(block):
            for j, char in enumerate(line): 
                if char == 'X':
                    if (y+i < 0) or (y+i >= height):
                        return False
                    if (x+j < 0) or (x+j >= width):
                        return False
                    if not self.CanPutOnPoint(y+i,x+j):
                        return False
        return True

    def rotate(self, block, mode):
        ret = []
        if mode == 0:
            ret = block
        elif mode == 1:
            for i in range(len(block[0])):
                ret.append([])
                for j in range(len(block)):
                    ret[i].insert(0, block[j][i])
        elif mode == 2:
            for i, line in enumerate(block):
                ret.insert(0, [])
                for j, char in enumerate(line):
                    ret[0].insert(0, char)
        elif mode == 3:
            for i in range(len(block[0])):
                ret.insert(0, [])
                for j in range(len(block)):
                    ret[0].append(block[j][i])
        elif mode == 4:
            for i, line in enumerate(block):
                ret.insert(0, [])
                for j, char in enumerate(line):
                    ret[0].append(char)
        elif mode == 5:
            for i in range(len(block[0])):
                ret.append([])
                for j in range(len(block)):
                    ret[i].append(block[j][i])
        elif mode == 6:
            for i, line in enumerate(block):
                ret.append([])
                for j, char in enumerate(line):
                    ret[i].insert(0, char)
        elif mode == 7:
            for i in range(len(block[0])):
                ret.insert(0, [])
                for j in range(len(block)):
                    ret[0].insert(0, block[j][i])
        return ret

    def PutBlock(self, nr, mode, x, y):
        block = self.blocks[nr]
        block = self.rotate(block, mode)
        return self.PutBlockOnBoard(block, x, y)
    
    def BlokiNaSiatceII(self):
        self.ClearBoard(WIDTH, HEIGHT)
        self.PutBlock(0, 2, 8, 3)
        self.PutBlock(1, 4, 2, 13)
        self.PutBlock(2, 3, 7, 11)
        self.PutBlock(3, 1, 4, 12)
        self.PutBlock(4, 4, 0, 3)
        self.PutBlock(5, 0, 5, 1)
        self.PutBlock(6, 6, 5, 8)
        self.PutBlock(7, 0, 1, 0)
        self.PutBlock(8, 1, 7, 7)
        self.PutBlock(9, 1, 3, 4)
        self.PutBlock(10, 1, 0, 8)
        self.PutBlock(11, 1, 8, 0)
    
    def RandomBoard(self):    
        self.ClearBoard(WIDTH, HEIGHT)
        TRIES = 1000
        for i in range(12):
            c = 0
            while (not (self.PutBlock(i, randint(0,7), randint(0,WIDTH-1), randint(0,HEIGHT-1))) and 
                  (c < TRIES)):
                c = c + 1
                if c == TRIES:
                    return False
        return True

    def BlokiTest(self):
        while not self.RandomBoard():
            pass

if __name__ == '__main__':
    board = BlocksBoard()
    board.BlokiNaSiatceII();
    #board.BlokiTest()
    board.ShowBoard(chr(219),' ', True, True)