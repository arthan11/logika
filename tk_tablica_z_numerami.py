from Tkinter import *
from tablica_z_numerami import NumbersTable 

class TkNumbersTable(NumbersTable):
    def ShowTableTk(self):
        master = Tk()

        size = 50
        x = size
        y = size
        width = len(self.table[0])
        height = len(self.table)

        w = Canvas(master, width= x+size*(width+1), height = y+size*(height+1) )
        w.pack()

        # wyswietlenie tabeli
        for i in range(width):
            for j in range(height):
                w.create_rectangle(x+i*size, y+j*size, x+(i+1)*size, y+(j+1)*size, fill='grey')

                if self.table[j][i] == None:
                    w.create_text(x+size*i+size/2, y+size*j+size/2, 
                                  text = '?', font = (None, 14))
                else:
                    w.create_text(x+size*i+size/2, y+size*j+size/2, 
                                  text = self.table[j][i], font = (None, 14)) 

        mainloop()

if __name__ == '__main__':
    table = TkNumbersTable()
    table.TablicaZNumerami();
    table.ShowTable()
    table.ShowTableTk();