import sys

class NumbersTable(object):
    def __init__(self):
        self.table = []
    
    def TablicaZNumerami(self):
        self.table = [[10, 3, 6, 7, None], [1, None, 5, 4, 9]]
    
    def ShowTable(self):
        for row in self.table:
            for nr in row:
                if nr == None:
                    print '?',
                else:
                    print nr, 
            print ''

if __name__ == '__main__':
    table = NumbersTable()
    table.TablicaZNumerami();
    table.ShowTable()